const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const news = require('./app/news');
const comments = require('./app/comments');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect('mongodb://localhost/News_Breaker',{useNewUrlParser: true}).then(()=>{
    app.use('/news',news);
    app.use('/comments',comments);

    app.listen(port,()=> console.log(`Server runned on ${port} port`));
});
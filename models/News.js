const mongoose = require('mongoose');

const NewsSchema = new mongoose.Schema({
    title:{
        type:String,
        required: true
    },
    text:{
        type:Text,
        required: true
    },
    photo:String,
    date:Date,
});

const News = mongoose.model('News',NewsSchema);
module.exports = News;
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    news:{
        type: Schema.Types.ObjectId,
        ref: 'News',
        required: true
    },
    comment_text:{
        type:String,
        required: true
    },
    author: String
});

const Comment = mongoose.model('Comment',CommentSchema);
module.exports = Comment;
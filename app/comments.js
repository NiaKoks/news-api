const express = require('express');
const Comment = require('../models/Comment');

const router = express.Router();

router.get('/',(req,res)=>{
    if (req.query.news) {
        Comment.find({news : req.query.news})
            .then(comments => res.send(comments))
            .catch(()=>res.sendStatus(500))
    }   Comment.find()
            .then(comments => res.send(comments))
            .catch(()=> res.sendStatus(500));
});
router.post('/',(req,res)=>{
    const comment = new Comment(req.body);
    comment.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

module.exports = router;
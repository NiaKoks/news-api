const express = require('express');
const News = require('../models/News');

const router = express.Router();

router.get('/',(req,res)=>{
    News.find()
        .then(news => res.send(news))
        .catch(()=> res.sendStatus(500));
});

router.post('/',(req,res)=>{
    const news = new News(req.body);
    news.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

module.exports = router;
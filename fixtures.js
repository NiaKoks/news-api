const mongoose = require('mongoose');

const News =require('./models/News');
// const Album =require('./models/Album');
const Comment =require('./models/Comment');
const config = require('./config');

const run =async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for(let collection of collections){
        await collection.drop();
    }

    const [news, news2,news3] = await News.create(
        {
            title: "Freddy Mercury",
            text: "Amazing news for everyone",
            photo: null,
            date: "20052018"
        },
        {
            title: "Breaking News",
            text: "Good news everyone",
            photo: null,
            date: "20102018"
        },
        {
            title: "Here is news title",
            text: "Amazing news for everyone! You wouldn't believe!",
            photo: null,
            date: "10022019"
        }
    );

    const [comment, comment2,comment3,comment4] = await Comment.create(
        {
            news: news._id,
            comment_text: "OMG! I'm shocked!!",
            author: "Betty",
        },
        {
            news: news._id,
            comment_text: "Amazing news!",
            author: "Petty",
        },
        {
            news: news2._id,
            comment_text: "Futurama reference",
            author: "Kitty",
        },
        {
            news: news3._id,
            comment_text: "OMG! I'm shocked!!",
            author: null,
        }
    );
    return connection.close();

};


run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});